angular.module('myApp').service('teamPlayerService', function(){
	var that = this;
	var team_players = [];
	var url = "http://localhost/FlunkyBallREST/team_player/get.php";
	$http({
		method : "GET",
		url : url
	}).then(function mySuccess(response) {
		console.log("Got data from "+url);
		for(i in response.data.records){
			var obj = response.data.records[i];
			var team_player = new TeamPlayer(obj.id,obj.id_player,obj.id_team);
			that.add(team_player);
		}
	//	team_players = response.data.records;
	}, function myError(response) {
		console.log(response.statusText);
	});
	this.getAll = function(){
		return team_players;
	}
	this.get = function(id){
		for(i in team_players){
			var team_player = team_players[i];
			if(team_player.id == id){
				return team_player;
			}
		}
	}
	this.add = function(team_player){
		if(this.get(team_player.id) == undefined){
			team_players.push(team_player);
		} else {
			console.log("Warning: Player already exists!");
		}
	}
});
