function Player() {
	this.id;
	this.name;
	this.user_id;
	this.team_ids = [];
	this.score; //needs calculation
}
function Player(id,name) {
	this.id = id;
	this.name = name;
	this.user_id;
	this.team_ids = [];
	this.score; //needs calculation
}

Player.prototype.addTeamId = function(team_id){
	if(this.team_ids.indexOf(team_id) == -1){
		this.team_ids.push(team_id);
	} else {
		console.log("Warning: could not add TeamId!");
	}
}

Player.prototype.toString = function(){
	if(this.user){
		return this.user.toString();
	} else {
		return this.name;
	}
}

Player.prototype.calcScore = function(team_service, game_service){
	return 3; //FIXME
	var score = 0;
	for(i in this.team_ids){
		var team_id = this.team_ids[i];
		var team = team_service.get(team_id);
		score += team.getScore(game_service);
		//TODO
	}
	return score;
}
