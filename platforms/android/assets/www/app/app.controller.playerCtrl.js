angular.module('myApp').controller("playerCtrl", function($scope, playerService) {
	$scope.players = playerService.getAll();
	$scope.$watch(playerService.getAll
		, function(val) {
			$scope.players = val;
		},true);
});
