angular.module('myApp').service('gameService', function($http){
	var that = this;
	var games = [];
	console.log("gameservice");
	var rest_server = "192.168.1.24";
	var url = "http://"+rest_server+"/FlunkyBallREST/game/get.php";
	$http({
		method : "GET",
		url : url
	}).then(function mySuccess(response) {
		console.log("Got data from "+url);
		for(i in response.data.records){
			var obj = response.data.records[i];
			var game = new Game(obj.id, obj.location, obj.date, obj.time);
			that.add(game);
		}
	//	games = response.data.records;
	}, function myError(response) {
		console.log(response.statusText);
	});
	this.getAll = function(){
		return games;
	}
	this.get = function(id){
		for(i in games){
			var game = games[i];
			if(game.id == id){
				return game;
			}
		}
	}
	this.add = function(game){
		if(this.get(game.id) == undefined){
			games.push(game);
		} else {
			console.log("Warning: Player already exists!");
		}
	}
});
