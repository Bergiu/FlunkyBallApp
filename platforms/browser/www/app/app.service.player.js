angular.module('myApp').service('playerService', function($http){
	var that = this;
	var players = [];
	var rest_server = "192.168.1.24";
	var rest_server = "185.82.216.57:5588"
	var url = "http://"+rest_server+"/FlunkyBallREST/player/get.php";
	$http({
		method : "GET",
		url : url
	}).then(function mySuccess(response) {
		console.log("Got data from "+url);
		for(i in response.data.records){
			var obj = response.data.records[i];
			var player = new Player(obj.id,obj.name);
			that.add(player);
		}
	//	players = response.data.records;
	}, function myError(response) {
		console.log(response.statusText);
	});
	this.getAll = function(){
		return players;
	}
	this.get = function(id){
		for(i in players){
			var player = players[i];
			if(player.id == id){
				return player;
			}
		}
	}
	this.add = function(player){
		if(this.get(player.id) == undefined){
			players.push(player);
		} else {
			console.log("Warning: Player already exists!");
		}
	}
});
