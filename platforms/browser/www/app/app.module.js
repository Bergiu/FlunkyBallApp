var app = angular.module("myApp",["ngRoute"]);

app.config(function($routeProvider, $locationProvider) {
	$locationProvider.hashPrefix('!');
	$routeProvider
	.when('/', {
		templateUrl : "view/main.htm",
		controller : "playerCtrl"
})
	.when('/list_games', {
		templateUrl : "view/list_games.htm",
		controller : "gameCtrl"

	})
	.when('/list_players', {
		templateUrl : "view/list_players.htm",
		controller : "playerCtrl"

	})
	.when('/new_game', {
		templateUrl : "view/new_game.htm"
	})
	.otherwise({
		template: "<h1>error</h1>"
	});
});
