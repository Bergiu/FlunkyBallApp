angular.module('myApp').service('teamService', function(){
	var that = this;
	var teams = [];
	var url = "http://localhost/FlunkyBallREST/team/get.php";
	$http({
		method : "GET",
		url : url
	}).then(function mySuccess(response) {
		console.log("Got data from "+url);
		for(i in response.data.records){
			var obj = response.data.records[i];
			var team = new Team(obj.id,obj.place,obj.name,obj.id_game);
			that.add(team);
		}
	//	teams = response.data.records;
	}, function myError(response) {
		console.log(response.statusText);
	});
	this.getAll = function(){
		return teams;
	}
	this.get = function(id){
		for(i in teams){
			var team = teams[i];
			if(team.id == id){
				return team;
			}
		}
	}
	this.add = function(team){
		if(this.get(team.id) == undefined){
			teams.push(team);
		} else {
			console.log("Warning: Player already exists!");
		}
	}
});
